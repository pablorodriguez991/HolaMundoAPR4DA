package inacap.test.holamundoapr4da.views;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import inacap.test.holamundoapr4da.R;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }
}
