package inacap.test.holamundoapr4da.views;

import android.content.ContentValues;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import inacap.test.holamundoapr4da.R;
import inacap.test.holamundoapr4da.controller.UsuariosController;
import inacap.test.holamundoapr4da.model.sqlite.HolaMundoDBContract;
import inacap.test.holamundoapr4da.model.sqlite.UsuariosModel;

/**
 * Created by mitlley on 23-08-17.
 */

public class FormularioActivity extends AppCompatActivity {

    private EditText etUsername, etPassword1, etPassword2;
    private Button btRegister;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Llamar al archivo XML
        setContentView(R.layout.activity_formulario);

        this.etUsername = (EditText) findViewById(R.id.etUsername);
        this.etPassword1 = (EditText) findViewById(R.id.etPassword1);
        this.etPassword2 = (EditText) findViewById(R.id.etPassword2);

        this.btRegister = (Button) findViewById(R.id.btRegister);

        this.btRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Guardamos mediante la capa controlador

                String username = etUsername.getText().toString();
                String password1 = etPassword1.getText().toString();
                String password2 = etPassword2.getText().toString();

                try {
                    UsuariosController controller = new UsuariosController(getApplicationContext());
                    controller.crearUsuario(username, password1, password2);
                } catch(Exception e){
                    String mensaje = e.getMessage();
                    Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT).show();
                }


            }
        });

    }
}














